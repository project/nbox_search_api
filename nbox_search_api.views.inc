<?php

/**
 * @file
 * Views functions for Nbox Search API.
 */

/**
 * Implements hook_views_data_alter().
 */
function nbox_search_api_views_data_alter(array &$data) {
  if (array_key_exists('search_api_index_nbox_messages', $data)) {
    $data['search_api_index_nbox_messages']['sender_relative'] = [
      'title' => \Drupal::translation()->translate('Sender relative'),
      'help' => \Drupal::translation()->translate('A relative sender.'),
      'field' => [
        'id' => 'nbox_search_api_view_sender_relative',
      ],
    ];
    $data['search_api_index_nbox_messages']['recipient_summary'] = [
      'title' => \Drupal::translation()->translate('Recipient summary'),
      'help' => \Drupal::translation()->translate('A recipient summary.'),
      'field' => [
        'id' => 'nbox_search_api_view_recipient_summary',
      ],
    ];
    $data['search_api_index_nbox_messages']['relative_date'] = [
      'title' => \Drupal::translation()->translate('Relative date'),
      'help' => \Drupal::translation()->translate('The relative sent date.'),
      'field' => [
        'id' => 'nbox_search_api_view_relative_date',
      ],
    ];
  }
}
