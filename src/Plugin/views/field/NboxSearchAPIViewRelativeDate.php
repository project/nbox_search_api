<?php

namespace Drupal\nbox_search_api\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * A handler to provide a field for the relative date.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("nbox_search_api_view_relative_date")
 */
class NboxSearchAPIViewRelativeDate extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_object;
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $entity->getValue();
    return $nbox->getSentTimeRelative();
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This function exists to override parent query function.
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    // Most recent is the incremental nbox message ID and thus highest ID, is
    // also most recent.
    $this->query->sort('sent_time', $order);
  }

}
