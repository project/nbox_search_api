<?php

namespace Drupal\nbox_search_api\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\nbox\Entity\NboxRelativityTrait;

/**
 * A handler to provide a field for the sender summary.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("nbox_search_api_view_recipient_summary")
 */
class NboxSearchAPIViewRecipientSummary extends FieldPluginBase {

  use NboxRelativityTrait;

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $values->_object;
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $entity->getValue();
    $recipients = array_merge(...array_values($nbox->getRecipients()));
    return implode(', ', $this->relativeUserNameMultiple($recipients));
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This function exists to override parent query function.
    // Do nothing.
  }

}
