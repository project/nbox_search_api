<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\user\Entity\User;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the participant names, bcc excluded, to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_participant_names",
 *   label = @Translation("Participant names"),
 *   description = @Translation("The names of all (non-bcc) participants in a message."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ParticipantNames extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];
    if ($datasource !== NULL && $datasource->getEntityTypeId() === 'nbox') {
      $definition = [
        'label' => $this->t('Participant names'),
        'description' => $this->t('The names of all the (non-bcc) participants in a message.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['participant_names'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $item->getOriginalObject()->getValue();
    $participants = [];
    $recipientsBcc = $nbox->getRecipientsWithBcc();
    foreach ($recipientsBcc as $fieldname => $recipientIds) {
      if ($fieldname !== 'bcc') {
        $participants[] = $recipientIds;
      }
    }
    $participants = array_merge(...$participants);
    array_unshift($participants, $nbox->getOwnerId());
    $users = User::loadMultiple($participants);
    $fields = $item->getFields(FALSE);
    $fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, 'entity:nbox', 'participant_names');
    foreach ($fields as $field) {
      foreach ($users as $user) {
        $field->addValue($user->getDisplayName());
      }
    }
  }

}
