<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\search_api\Query\ResultSetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters message where the user is a participant.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_participant_filter",
 *   label = @Translation("Participant filter"),
 *   description = @Translation("Filter messages to show only where user is a participant."),
 *   stages = {
 *     "preprocess_query" = 0,
 *     "postprocess_query" = 0,
 *   }
 * )
 */
class ParticipantFilter extends ProcessorPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The current_user service used by this plugin.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface|null
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->setCurrentUser($container->get('current_user'));
    return $processor;
  }

  /**
   * Sets the current user.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   *
   * @return $this
   */
  public function setCurrentUser(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
    return $this;
  }

  /**
   * Retrieves the current user.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   *   The current user.
   */
  public function getCurrentUser() {
    return $this->currentUser ?: \Drupal::currentUser();
  }

  /**
   * Can only be enabled for an index that indexes the nbox entity.
   *
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    foreach ($index->getDatasources() as $datasource) {
      if ($datasource->getEntityTypeId() === 'nbox') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'bcc' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['bcc'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Respect BCC in results'),
      '#description' => $this->t("If set, don't search in bcc names that you are not allowed to see."),
      '#default_value' => $this->configuration['bcc'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values['bcc'] = (bool) $values['bcc'];
    $form_state->set('values', $values);
    $this->setConfiguration($values);
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessSearchQuery(QueryInterface $query) {
    parent::preprocessSearchQuery($query);
    $this->getCurrentUser();
    $query->addCondition('participant_uids', $this->getCurrentUser()->id(), 'IN');
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    parent::postprocessSearchResults($results);
    if ($this->configuration['bcc']) {
      $result_items = $results->getResultItems();
      $fields_by_datasource = [];
      foreach ($this->index->getFields() as $field_id => $field) {
        if (in_array($field_id, ['bcc_uids', 'sender_uid'])) {
          $fields_by_datasource[$field->getDatasourceId()][$field->getPropertyPath()] = $field_id;
        }
      }
      $result_fields = $this->getFieldsHelper()
        ->extractItemValues($result_items, $fields_by_datasource);
      foreach ($result_fields as $result_key => $result_field) {
        if (count($result_field['bcc_uids']) > 0 && (!in_array($this->currentUser->id(), $result_field['sender_uid']) && !in_array($this->currentUser->id(), $result_field['bcc_uids']))) {
          unset($result_items[$result_key]);
        }
      }
      $results->setResultItems($result_items);
    }
  }

}
