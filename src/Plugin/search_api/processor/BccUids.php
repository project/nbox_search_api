<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the bcc uids to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_bcc_uids",
 *   label = @Translation("BCC UIDs"),
 *   description = @Translation("The uids of all the bcc's in a message."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class BccUids extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];
    if ($datasource !== NULL && $datasource->getEntityTypeId() === 'nbox') {
      $definition = [
        'label' => $this->t('BCC UIDs'),
        'description' => $this->t("The uids of all the bcc's in a message."),
        'type' => 'integer',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['bcc_uids'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $item->getOriginalObject()->getValue();
    $bccs = [];
    $recipientsBcc = $nbox->getRecipientsWithBcc();
    foreach ($recipientsBcc as $fieldname => $recipientIds) {
      if ($fieldname === 'bcc') {
        $bccs[] = $recipientIds;
      }
    }
    if (count($bccs) > 0) {
      $bccs = array_merge(...$bccs);
      $fields = $item->getFields(FALSE);
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($fields, 'entity:nbox', 'bcc_uids');
      foreach ($fields as $field) {
        foreach ($bccs as $bcc) {
          $field->addValue($bcc);
        }
      }
    }
  }

}
