<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\ResultSetInterface;

/**
 * Group messages per thread.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_thread_group",
 *   label = @Translation("Group per thread"),
 *   description = @Translation("Group messages per thread ID."),
 *   stages = {
 *     "postprocess_query" = 100,
 *   }
 * )
 */
class ThreadGroup extends ProcessorPluginBase {

  /**
   * Can only be enabled for an index that indexes the nbox entity.
   *
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    foreach ($index->getDatasources() as $datasource) {
      if ($datasource->getEntityTypeId() === 'nbox') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    parent::postprocessSearchResults($results);
    $resultItems = $results->getResultItems();
    $fields_by_datasource = [];
    foreach ($this->index->getFields() as $field_id => $field) {
      if (in_array($field_id, ['nbox_thread_id', 'nbox_id'])) {
        $fields_by_datasource[$field->getDatasourceId()][$field->getPropertyPath()] = $field_id;
      }
    }
    $result_fields = $this->getFieldsHelper()
      ->extractItemValues($resultItems, $fields_by_datasource);
    $uniqueThreads = [];
    foreach ($result_fields as $result_key => $result_field) {
      $thread = reset($result_field['nbox_thread_id']);
      $message = reset($result_field['nbox_id']);
      if (array_key_exists($thread, $uniqueThreads) && $uniqueThreads[$thread]['nbox_id'] > $message) {
        continue;
      }
      else {
        $uniqueThreads[$thread] = [
          'nbox_id' => $message,
          'key' => $result_key,
        ];
      }
    }

    foreach (array_keys($resultItems) as $key) {
      if (!in_array($key, array_column($uniqueThreads, 'key'))) {
        unset($resultItems[$key]);
      }
    }
    $results->setResultItems($resultItems);
  }

}
