<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the participant uids to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_participant_uids",
 *   label = @Translation("Participant UIDs"),
 *   description = @Translation("The uids of all the participants in a message."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ParticipantUids extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];
    if ($datasource !== NULL && $datasource->getEntityTypeId() === 'nbox') {
      $definition = [
        'label' => $this->t('Participant UIDs'),
        'description' => $this->t('The uids of all the participants in a message.'),
        'type' => 'integer',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['participant_uids'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $item->getOriginalObject()->getValue();
    $participants = array_merge(...array_values($nbox->getRecipientsWithBcc()));
    array_unshift($participants, $nbox->getOwnerId());
    $fields = $item->getFields(FALSE);
    $fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, 'entity:nbox', 'participant_uids');
    foreach ($fields as $field) {
      foreach ($participants as $participant) {
        $field->addValue($participant);
      }
    }
  }

}
