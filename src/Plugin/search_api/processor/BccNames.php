<?php

namespace Drupal\nbox_search_api\Plugin\search_api\processor;

use Drupal\user\Entity\User;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the bcc names to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "nbox_search_api_bcc_names",
 *   label = @Translation("BCC names"),
 *   description = @Translation("The names of all the bcc's in a message."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class BccNames extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];
    if ($datasource !== NULL && $datasource->getEntityTypeId() === 'nbox') {
      $definition = [
        'label' => $this->t('BCC names'),
        'description' => $this->t('The names of all the bcc recipients in a message.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['bcc_names'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    /** @var \Drupal\nbox\Entity\Nbox $nbox */
    $nbox = $item->getOriginalObject()->getValue();
    $bccs = [];
    $recipientsBcc = $nbox->getRecipientsWithBcc();
    foreach ($recipientsBcc as $fieldname => $recipientIds) {
      if ($fieldname === 'bcc') {
        $bccs[] = $recipientIds;
      }
    }
    $bccs = array_merge(...$bccs);
    if (count($bccs) > 0) {
      $users = User::loadMultiple($bccs);
      $fields = $item->getFields(FALSE);
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($fields, 'entity:nbox', 'bcc_names');
      foreach ($fields as $field) {
        foreach ($users as $user) {
          $field->addValue($user->getDisplayName());
        }
      }
    }
  }

}
